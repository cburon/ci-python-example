# CI-CD Python Unittest
The program provides two ways to solve the gaussian sum formula (Der kleine Gauß). With unit tests.

# Unit test for the functions 
```bash
python3 test_gaussian_sum_formula.py
```

# Create documentation 
```bash
pydoc3 -w gaussian_sum_formula
```

# Linting Test 
```bash
pylint gaussian_sum_formula.py

```
