import unittest
from gaussian_sum_formula import loop, formula

class TestCircleArea(unittest.TestCase):
    def test_area(self):
        self.assertAlmostEqual(loop(100), 5050)
        self.assertAlmostEqual(formula(100), 5050)

    def test_values(self):
        self.assertRaises(ValueError, loop, -2)
        self.assertRaises(ValueError, formula, -2)

    def test_types(self):
        self.assertRaises(TypeError, formula, "foobar")
        self.assertRaises(TypeError, loop, "foobar")

if __name__ == '__main__':
    unittest.main()
